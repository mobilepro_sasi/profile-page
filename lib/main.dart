import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.teal.shade800,
        ));
  }

  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.black,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.teal.shade800,
        ));
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  bool click = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.LIGHT
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppbarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              click = !click;
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
          child: click ? Icon(Icons.nightlight) : Icon(Icons.sunny),
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.teal.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          // color: Colors.teal.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.teal.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.teal.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.teal.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.teal.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

//ListTile
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("061-174-3166"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      // color: Colors.teal.shade800,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      // color: Colors.teal.shade800,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160287@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing: Text(""),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("Samutprakan, Thailand"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      // color: Colors.teal.shade800,
      onPressed: () {},
    ),
  );
}

AppBar buildAppbarWidget() {
  return AppBar(
    backgroundColor: Colors.teal,
    leading: Icon(Icons.arrow_back),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(Icons.star_border),
      )
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 350,

            child: Image.network(
            "https://lh3.googleusercontent.com/chat_attachment/AJh6FppLfC-UfEzENTL5QzfNbRDR1dnlWXE1FzW69s0We2HVLwJv80lR5wkV0itEgCsGQMYGHebOoQ7531LP9zB3yP5-Jp7Ca4pin-2ENgKorf0Jcao3l_WyFr_xzoYHc1XdZEaGK8uxAfBbsFrdySOInVHZG9_LUvYz7sZnkARn08Ldn0C2RONw0vz32tFE54_nrBPv61OWxDquvPLtIkUnt4Keubk=w1366-h657",
            fit: BoxFit.cover,
          ),
          ),
        ],
      ),
      Container(
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Sasina Mumao",
                style: TextStyle(fontSize: 30),
              ),
            )
          ],
        ),
      ),
      Divider(
        color: Colors.grey,
        thickness: 0.8,
      ),
      Container(
          margin: EdgeInsets.only(top: 8, bottom: 8),
          child: Theme(
            data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.teal.shade500,
                )),
            child: profileActionItems(),
          )),
      Divider(
        color: Colors.grey,
        thickness: 0.5,
      ),
      mobilePhoneListTile(),
      // otherPhoneListTile(),
      Divider(
        color: Colors.grey,
        thickness: 0.3,
      ),
      emailListTile(),
      addressListTile(),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}
